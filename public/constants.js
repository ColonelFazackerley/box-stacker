"use strict";

/**
 * Set up at the very beginning and never updated.
 */
class Constants {
    shapeDefs = null;
    dynObjDefs = null;
    gravity = Vec2(0.0, 10.0);
    timeStep = 1.0 / 60.0;
    velocityIterations = 6;
    positionIterations = 2;
}
