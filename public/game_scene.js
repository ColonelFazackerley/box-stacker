"use strict";

const BACKGROUND_PNG_SIZE = 500;
const LESS_DETAIL_BUTTON_SVG_SIZE = 450;
const MINUS_SVG_SIZE = 100;
const MENU_BUTTON_SVG_SIZE = 100;
const MENU_BACKGROUND_SVG_SIZE = 500;
const MORE_DETAIL_BUTTON_SVG_SIZE = 450;
const PLUS_SVG_SIZE = 100;

const GameScene = new Phaser.Class({
    Extends: Phaser.Scene,

    selectedShape: null,
    draggedShape: null,
    dragStart: null,
    dragMoved: null,
    ghostShape: null,
    pointer1: null,
    pointer2: null,

    level: null,

    initialize: function GameScene() {
        Phaser.Scene.call(this, { key: 'GameScene', active: true });
    },

    /**
     * @param {string} name
     */
    loadj: function(name) {
        this.load.json(name, name + '.json');
    },

    preload: function() {
        this.loadj('shape_def');
        this.loadj('dynamic_object_def');

        this.load.spritesheet(
            'diamondblockred',
            'images/diamondblockred.svg',
            { frameWidth: 1132 }
        );
        this.load.spritesheet(
            'diamondblockblue',
            'images/diamondblockblue.svg',
            { frameWidth: 1132 }
        );
        this.load.spritesheet(
            'diamondblockgreen',
            'images/diamondblockgreen.svg',
            { frameWidth: 1132 }
        );
        this.load.spritesheet(
            'diamondblockyellow',
            'images/diamondblockyellow.svg',
            { frameWidth: 1132 }
        );
        this.load.spritesheet(
            'slope_1',
            'images/slope_1.svg',
            { frameWidth: 1132 }
        );
        this.load.spritesheet(
            'slope_2',
            'images/slope_2.svg',
            { frameWidth: 1132 }
        );
        this.load.spritesheet(
            'slope_3',
            'images/slope_3.svg',
            { frameWidth: 1132 }
        );
        this.load.spritesheet(
            'slope_4',
            'images/slope_4.svg',
            { frameWidth: 1132 }
        );
        this.load.image('wheel', 'images/wheel.svg');
        this.load.image('carbase', 'images/carbase.svg');
        this.load.image('dynamicbox', 'images/dynamicbox.svg')
        this.load.image('dynamicball', 'images/dynamicball.svg')
        this.load.image('dynamicballbouncy', 'images/dynamicballbouncy.svg')
        this.load.image('dynamicfloating', 'images/dynamicfloating.svg')
        this.load.image('anvil', 'images/anvil.svg')
        this.load.image('woodblockunpainted', 'images/woodblockunpainted.svg');
        this.load.image('balloons', 'images/balloons.svg');
    },

    create: function() {
        app_state.gameScene = this;
        const sd_result = this.cache.json.get('shape_def');
        const dynObjDefs_result = this.cache.json.get('dynamic_object_def');
        constants.shapeDefs = jsonShapeDefsToJS(sd_result);
        constants.dynObjDefs = jsonDynObjDefsToJS(dynObjDefs_result);

        const make_lazy_level = (filename) => {
            const level = new LazyLevel(filename);
            // This is async but we don't await it - we want to
            // do it in the background - fetch the level JSON.
            level.get();
            return level;
        }

        app_state.levels = level_names.map(make_lazy_level);
        app_state.levels_completed = load_levels_completed();
        app_state.current_level_num = load_current_level_num();
        app_state.levels_menu_offset = find_levels_menu_offset(
            app_state.current_level_num
        );

        if (document.location.hash.startsWith('#play=')) {
            try {
                const level_id = parseInt(
                    document.location.hash.substring("#play=".length)
                );
                do_download_to_play(level_id);
            } catch (e) {
                console.error(e);
                setup_title(this);
            }
        } else if (document.location.hash.startsWith('#solution=')) {
            try {
                const level_id = parseInt(
                    document.location.hash.substring("#solution=".length)
                );
                do_download_to_view_solution(level_id);
            } catch (e) {
                console.error(e);
                setup_title(this);
            }
        } else {
            setup_title(this);
        }

        this.input.addPointer();

        this.input.keyboard.on('keydown', (e) => {
            const tag = document.activeElement.tagName;
            if (tag === "INPUT" || tag === "SELECT" || tag === "TEXTAREA") {
                return;
            }
            switch(e.key) {
                case ' ':
                    e.preventDefault();
                    return space_pressed();
                case 'Escape':
                    e.preventDefault();
                    return esc_pressed();
                case 'ArrowLeft':
                    e.preventDefault();
                    return left_pressed();
                case 'Delete':
                    e.preventDefault();
                    return delete_pressed();
                case 'f':
                    e.preventDefault();
                    return f_pressed();
                case 'h':
                    e.preventDefault();
                    return h_pressed();
                case 'r':
                    e.preventDefault();
                    return r_pressed();
                case '-':
                    e.preventDefault();
                    return minus_pressed();
                case '=':
                    e.preventDefault();
                    return equals_pressed();
            }
        });

        this.cameras.main.setZoom(1);
        this.cameras.main.setBounds(
            -builderState.resolution,
            -builderState.resolution,
            builderState.resolution * 3,
            builderState.resolution * 3
        );

        this.input.on("pointerdown", function (pointer) {
            if (
                app_state.mode === APP_STATES.building ||
                app_state.mode === APP_STATES.focus_building ||
                app_state.mode === APP_STATES.editing ||
                app_state.mode === APP_STATES.focus_editing
            ) {
                start_drag(this, pointer);
            }
        }, this);

        this.input.on("pointerup", function (pointer) {
            if (this.pinch_zooming) {
                this.pinch_zooming = false;
                return;
            }
            this.draggedShape = null;
            if (
                app_state.mode === APP_STATES.building ||
                app_state.mode === APP_STATES.focus_building ||
                app_state.mode === APP_STATES.editing ||
                app_state.mode === APP_STATES.focus_editing
            ) {
                const distX = pointer.x - pointer.downX;
                const distY = pointer.y - pointer.downY;
                const distsq = distX * distX + distY * distY;
                if (distsq < 20) {
                    place_block(this, pointer);
                }
            }
        }, this);

        this.input.on(
            'pointermove',
            (pointer, _currentlyOver) => this.on_pointermove(pointer)
        );

        this.input.on('wheel', (_pointer, _objs, _dx, dy, _dz) => {
            this.events.emit('zoom', dy < 0 ? 1.2 : 1 / 1.2);
        });

        this.events.on('zoom', (amount) => {
            if (
                app_state.mode !== APP_STATES.building &&
                app_state.mode !== APP_STATES.focus_building &&
                app_state.mode !== APP_STATES.solution &&
                app_state.mode !== APP_STATES.editing &&
                app_state.mode !== APP_STATES.focus_editing
            ) {
                return;
            }
            let z = this.cameras.main.zoom * amount;
            if (z > 4) {
                z = 4;
            } else if (z < 1/3) {
                z = 1/3;
            }
            this.cameras.main.zoom = z;
        });

        this.events.on('start', () => {
            builderState.gameState = currentLevel.gameState.snapshot();
            builderState.zoom = this.cameras.main.zoom;
            builderState.scrollX = this.cameras.main.scrollX;
            builderState.scrollY = this.cameras.main.scrollY;

            app_state.mode = APP_STATES.running;
            this.selectedShape = null;
            this.draggedShape = null;
            const planckWorld = currentLevel.gameState.planckWorld;
            clear_planck_world(planckWorld);
            clear_phaser_scene(this);
            currentLevel.populateLevel(this, planckWorld);
            joinTiles();
            app_state.run();
        });

        this.events.on('reset', () => {
            const planckWorld = currentLevel.gameState.planckWorld;
            currentLevel.gameState = builderState.gameState;
            this.cameras.main.zoom = builderState.zoom;
            this.cameras.main.scrollX = builderState.scrollX;
            this.cameras.main.scrollY = builderState.scrollY;

            builderState.gameState = null;
            builderState.scrollX = null;
            builderState.scrollY = null;

            clear_planck_world(planckWorld);
            clear_phaser_scene(this);
            currentLevel.populateLevel(this, planckWorld);
            if (app_state.viewing_solution) {
                app_state.mode = app_state.prev_mode;
                app_state.viewing_solution = false;
            } else {
                app_state.mode = APP_STATES.building;
            }
        });
    },

    on_pointermove(pointer) {
        this.pinch_zooming = false;
        if (pointer.id === 1) {
            this.pointer1 = pointer;
            this.pinch_zooming = this.maybePinchZoom();
        } else if (pointer.id === 2) {
            this.pointer2 = pointer;
            this.pinch_zooming = this.maybePinchZoom();
        } else {
            this.pointer1 = pointer;
        }
        if (this.pinch_zooming) {
            return;
        }

        if (
            app_state.mode !== APP_STATES.building &&
            app_state.mode !== APP_STATES.focus_building &&
            app_state.mode !== APP_STATES.solution &&
            app_state.mode !== APP_STATES.editing &&
            app_state.mode !== APP_STATES.focus_editing
        ) {
            return;
        }
        if (pointer.isDown) {
            if (this.ghostShape) {
                this.ghostShape.destroy();
                this.ghostShape = null;
            }

            const dx = pointer.position.x - pointer.prevPosition.x;
            const dy = pointer.position.y - pointer.prevPosition.y;
            if (
                this.draggedShape &&
                (
                    app_state.mode === APP_STATES.building ||
                    app_state.mode === APP_STATES.focus_building ||
                    app_state.mode === APP_STATES.editing ||
                    app_state.mode === APP_STATES.focus_editing
                )
            ) {
                if (dx !== 0 || dy !== 0) {
                    if (this.selectedShape !== this.draggedShape) {
                        this.selectedShape = this.draggedShape;
                    }
                    move_or_resize_shape(this, this.draggedShape, dx, dy);
                }
            } else {
                const cam = this.cameras.main;
                const scale = cam.zoom;
                cam.scrollX -= dx / scale;
                cam.scrollY -= dy / scale;
            }
        } else {
            if (
                app_state.mode === APP_STATES.building ||
                app_state.mode === APP_STATES.focus_building ||
                app_state.mode === APP_STATES.editing ||
                app_state.mode === APP_STATES.focus_editing
            ) {
                if (this.ghostShape) {
                    this.moveGhostShape(pointer, this);
                } else {
                    this.createGhostShape(pointer, this);
                }

                const position = this.screenToPlanck(pointer);
                document.body.style.cursor = cursor_for_position(position);
            }
        }
    },

    maybePinchZoom() {
        const do_pinch_zoom = (
            this.pointer1 &&
            this.pointer1.isDown &&
            this.pointer2 &&
            this.pointer2.isDown
        );

        if (do_pinch_zoom) {

            const prevDist = this.pointer1.prevPosition.distance(
                this.pointer2.prevPosition
            );

            const newDist = this.pointer1.position.distance(
                this.pointer2.position
            );

            const zoomFactor = newDist/prevDist;

            if (
                Number.isFinite(zoomFactor)
                && Math.abs(zoomFactor - 1) < 0.1
            ) {
                this.events.emit("zoom", zoomFactor);
            }
        }
        return do_pinch_zoom;
    },

    createGhostShape(pointer) {
        if (!pointer) {
            pointer = Vec2(this.pointer1.x, this.pointer1.y);
        }

        let current_tool;
        let current_orientation;

        if (
            app_state.mode === APP_STATES.building ||
            app_state.mode === APP_STATES.focus_building
        ) {
            current_tool = builderState.currentShape;
            current_orientation = builderState.current_orientation;
        } else if (
            app_state.mode === APP_STATES.editing ||
            app_state.mode === APP_STATES.focus_editing
        ) {
            current_tool = editorState.current_tool;
            current_orientation = editorState.current_orientation;
        }

        // Only hover for shapes
        if (current_tool >= 100) {
            return;
        }

        // In building mode, no hover if we've run out
        if (
            app_state.mode === APP_STATES.building ||
            app_state.mode === APP_STATES.focus_building
        ) {
            const shapes_used = currentLevel.gameState.shapes_used;
            const num_used = shapes_used[current_tool] ?? 0;
            const original_num = (
                currentLevel.find_shape(current_tool)?.number ?? 0
            );
            const num_left = original_num - num_used;
            if (original_num !== -1 && num_left === 0) {
                return;
            }
        }

        const shape_def = constants.shapeDefs[current_tool];
        const shape_squares = rotated_shape_def(
            current_tool,
            current_orientation
        );

        const position = this.screenToPlanck(pointer);

        let snapped_pos = Vec2(
            Math.floor(position.x),
            Math.floor(position.y)
        );

        this.ghostShape = this.add.container(
            planckToPhaserX(snapped_pos.x + 0.5),
            planckToPhaserY(snapped_pos.y + 0.5),
            []
        );

        const colour = shape_def[1];
        const image = shape_def[2];

        for (const square of shape_squares) {
            let phaserShape;

            const pos = square[0];
            const type = square[1];
            if (type === "full") {
                phaserShape = populate_full_square(
                    this,
                    pos,
                    colour,
                    true,
                    image
                ).phaserShape;
            } else if (type === "slope") {
                phaserShape = populate_slope_square(
                    this,
                    pos,
                    colour,
                    current_orientation,
                    true,
                    image
                ).phaserShape;
            }

            this.ghostShape.add(phaserShape);
        }
    },

    moveGhostShape(pointer) {
        const position = this.screenToPlanck(pointer);

        let snapped_pos = Vec2(
            Math.floor(position.x),
            Math.floor(position.y)
        );

        let formatted_position = Vec2(
            planckToPhaserX(snapped_pos.x + 0.5),
            planckToPhaserY(snapped_pos.y + 0.5)
        );

        this.ghostShape.setPosition(
            formatted_position.x,
            formatted_position.y
        );
    },

    /**
     * @param {{x: number, y: number, w: number} | null | undefined} extent
     */
    zoom_to(extent) {
        if (!extent) {
            extent = { x: 0, y: 0, w: 20 };
        }
        const zoom = 20 / extent.w;
        const x = planckToPhaserX(extent.x);
        const y = planckToPhaserY(extent.y);

        this.cameras.main.zoom = zoom;
        this.cameras.main.scrollX = x;
        this.cameras.main.scrollY = y;
    },

    /**
     * @returns { x: number, y: number, w: number }
     */
    screen_zoom_as_planck() {
        const camera = this.cameras.main;
        return {
            w: 20 / camera.zoom,
            x: phaserToPlanckX(camera.scrollX),
            y: phaserToPlanckY(camera.scrollY)
        };
    },

    update: function(time, delta) {
        if (
            app_state.mode === APP_STATES.running ||
            app_state.mode === APP_STATES.title ||
            app_state.mode === APP_STATES.choose_level
        ) {
            update(time, delta);
        }

        if (!currentLevel.gameState.planckWorld) {
            return;
        }

        // Move Phaser objects to match the position of their Planck object
        for (
            let b = currentLevel.gameState.planckWorld.getBodyList();
            b;
            b = b.getNext()
        ) {
            let bodyPosition = b.getPosition();
            let bodyAngle = b.getAngle();
            let phaserObject = b.getUserData();
            if (phaserObject) {
                phaserObject.x = planckToPhaserX(
                    bodyPosition.x + (b.adjustX ?? 0)
                );
                phaserObject.y = planckToPhaserY(bodyPosition.y);
                phaserObject.rotation = bodyAngle;
            }
        }

        for (const shape of currentLevel.all_shapes()) {
            shape.update_selected(this.selectedShape, this);
        }
    },

    screenToPlanck: function(screenPos) {
        const worldPoint = this.cameras.main.getWorldPoint(
            screenPos.x,
            screenPos.y
        );
        return new Vec2(
            ( worldPoint.x / builderState.resolution ) * 20,
            ( worldPoint.y / builderState.resolution ) * 20
        );
    },

    screenToPlanckDelta: function(screenDelta) {
        return (
            screenDelta / (
                builderState.resolution * this.cameras.main.zoom
            )
        ) * 20;
    },
});
