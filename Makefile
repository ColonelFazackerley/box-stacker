all:
	@echo "Try 'make upload'"

upload:
	rsync \
		-r --delete \
		public/ \
		box-stacker.artificialworlds.net:box-stacker.artificialworlds.net/game/

upload-alpha:
	mv public/version.js public/real-version.js
	echo 'const VERSION = "alpha-'$$(date +'%Y%m%d-%H%M%S')'";' > public/version.js
	cat public/version.js
	rsync \
		-r --delete \
		public/ \
		box-stacker.artificialworlds.net:box-stacker.artificialworlds.net/game-alpha/
	mv public/real-version.js public/version.js

