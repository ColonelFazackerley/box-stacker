# Box Stacker

Box Stacker is a construction puzzle game.

## License

Copyright 2022 Andy Balaam, Codesmith00, RayDuck and the Box Stacker
contributors.

Released under the AGPLv3 license. See [LICENSE](LICENSE) for info.

## Code of conduct

Please note that this project is released with a
[Contributor Code of Conduct](code_of_conduct.md). By participating in this
project you agree to abide by its terms.

[![Contributor Covenant](contributor-covenant-v2.0-adopted-ff69b4.svg)](code_of_conduct.md)

In addition, this project is child-friendly, so please be extra-careful to be
polite, understanding and respectful at all times.
