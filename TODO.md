## Release 1.0.0

- [x] ShapeBlock.populate - rename some of the many things called shapeDef
- [x] Break up ShapeBlock.populate
- [x] Resolve inconsistency between shape_id and type
- [x] Move more code inside classes like ShapeBlock
- [x] Consider passing Planck and Phaser worlds into populate
- [x] Title screen and menu
- [x] Drags too fast when zoomed
- [x] Reset zoom when leaving level, and whether we're running etc.
- [x] Windy can't be turned into JSON
- [x] createPlanckObjFromForm actually creates a Body
- [x] Zoom out when you click run
- [x] Remove most buttons when you click run
- [x] Allow zooming further out
- [x] Zoom with mouse wheel
- [x] Menu button on each page, accessible via Esc
- [x] Finish normal play screen
- [x] Level editor
- [x] Back button on running mode should just reset
- [x] Reset scroll pos as well as zoom when resetting
- [x] Show time in running mode
- [x] Icon for car
- [x] Icon for ground
- [x] Graphics for ground
- [x] Fix the fact that slopes don't stick
- [x] Save (and load) buttons on save screen are confusing. Remove.
- [x] Fix crash with negative y values
- [x] Allow [ and ] in level names
- [x] Prevent HTML injection from level notes
- [x] Background image is not big enough
- [x] Allow each level to specify default zoom
- [x] Upload solutions
- [x] Dynamic object icons should not rotate
- [x] Clear all button in level editor (in menu, renamed from settings)
- [x] Reset button in builder
- [x] Add author nickname to save and upload dialogs (and level properties)
- [x] Limited numbers of each shape
- [x] Edit level properties in editor
- [x] Levels visible inside forum
- [x] Keyboard shortcuts
- [x] iOS Icons are small
- [x] New dynamic objects (ball, square, left-facing car)
- [x] Available shapes button in level editor menu
- [x] Dragging cars is off
- [x] Shapes don't slot into right-shaped holes (spoils level 2)
- [x] The car doesn't have enough power to drive up a slope
- [x] Stop it swallowing key strokes
- [x] Nickname defaults to undefined, and should be saved/loaded in both places
- [x] Add time and number of blocks to solution comment
- [x] Ensure forum post title is long enough
- [x] Anvil
- [x] Make lava use an anvil
- [x] Allow playing solutions in chemode
- [x] Can't select/move/delete blocks that are part of the level design
- [x] Allow overwriting existing local saved edited levels
- [x] Balloon platform
- [x] Slopes stick to the wrong side
- [x] Fix bug where there is no hover in level editor when the number=0
- [x] Balloon texture
- [x] Fix balloons stay around when float is deleted
- [x] Anvil is not in the selector
- [x] Prevent error if you click things too early
- [x] Create 30 levels
- [x] Check all the solutions work
- [x] Check all levels have notes and hints
- [x] Check for authors.
- [x] Add levels to forum + secret solutions
- [x] Prevent zoom on double-tap
- [x] Attempt to fix iOS small screen issues

## Release 1.0.1

- [X] Make the level timings as ticks / 60
- [X] Make the solution player zoom to fit all blocks
- [X] Have link to forum on front page
- [X] Have version number on front page

## Release 1.0.2

- [x] Fix bug in solution view

## Release 1.0.3

- [x] Prevent crash when there are no shapes
- [x] Remember camera zoom+location when you re-enter building mode
- [x] Remember camera zoom+location when you re-enter editing mode
- [x] Fix: Crazy golf solution uses blocks you are not allowed
- [x] Fix: In Up the instructions say "car" when they should say "ball"

## Release 1.1.0

- [x] Level editor picks up your zoom level when you save - no manual entry
- [x] Focus mode
- [x] Increase max manual zoom to be consistent with max used in levels
- [x] When zooming to fit a solution, don't zoom closer than the original zoom

## Release 1.1.1

- [x] Save zoom level from editor when you click play

## Release 1.1.2

- [x] Fix iOS Safari squashed buttons

## Release 1.2

- [x] Don't count blocks that you didn't place

## Release 1.2.1

- [x] Lazy-load levels

## Release 1.2.2

- [x] Pre-fetch all levels asynchronously

## Release 1.3.0

- [x] Make the first ~5 levels a really good intro to the game

## Release 1.3.1

- [x] Fix bug causing 'TypeError: level.get is not a function'

## Public release

- [ ] Split level selection into easy, medium, hard, extreme (but "next" button
  and jumping to the current level still work as before)
- [ ] Add new levels from forum
- [ ] Clicking the background dismisses dialogs
- [ ] Drag and drop from buttons into game should work
- [ ] Add smolpxl banner
- [ ] Add play-tracking stats

## Later

### Small

- [ ] Ensure exits are visible when viewing solutions
- [ ] After Reset Level I can't Show Solution
- [ ] Can't see remaining count on blue blocks
- [ ] Allow zooming in play mode
- [ ] "Open in level editor" button when playing
- [ ] Green and red rectangles should have a clear border but less opaque inside
- [ ] Better button icons
- [ ] Icons for menu options
- [ ] Add a Next Level button to the after-upload screen
- [ ] Link after uploading solution should link to the exact forum post, not the
  whole discussion
- [ ] Allow editing wind in level editor
- [ ] Link to forum for level from somewhere in game
- [ ] Allow uploading heroic failures

### Medium

- [ ] ES6 modules
- [ ] Support "and" conditions in editor with a single checkbox
- [ ] On title screen, don't show buttons until they are clickable
- [ ] Investigate the weirdness of zooming: see tests in game.js for clues
- [ ] Fix the glitch with slope textures when you are very zoomed out
- [ ] Better graphics for slopes
- [ ] Grid Background
- [ ] Fix bug where id goes back to "main_car" somewhere, whenever I add an
  "and" condition.
- [ ] Direct link should display with a preview in Twitter/Mastodon etc.
- [ ] Provide the direct link to a level or solution, not just forum link
- [ ] Disable delete button if you can't delete
- [ ] Exclusion zones where you can't place blocks
- [ ] Don't delete and recreate menu items
- [ ] shape_block.type is "id" in JSON
- [ ] Car's type is dynamic_object_def's "name"

### Big

- [ ] Fix: Slopes don't stick to balloons (WILL THIS BREAK EXISTING SOLUTIONS?)
- [ ] Maybe rotate should rotate the currently-selected block
- [ ] Make browser show there is a new update and tell the user to refresh
    - Probably use fetch("version.js", {"cache": "no-cache"}) and compare file
      contents with `const VERSION = "${VERSION}";\n`. If no-cache does not work
      (apparently it doesn't in Chrome?) then we can append `?cachebuster=${ts}`
      to the URL. If it doesn't match, refresh the page.
    - Do this check every 24 hours, and onload
    - Make sure it does nothing if you have no Internet, or there are errors
    - Make extra sure we don't get into a refresh loop
- [ ] Sandbox and challenges
- [ ] Piston blocks
- [ ] Triggers and timers
