# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.3.1] - 2023-02-04

### Fixed

- Fix bug causing 'TypeError: level.get is not a function'

## [1.3.0] - 2023-02-04

### Changed

- Re-jig the first five levels to make a clearer intro to how to play
- Replace Mountain Climb with Mountain Climber, which is a tiny bit harder
- Add some gifs that show how to play

## [1.2.2] - 2023-02-01

### Changed

- Pre-fetch all levels at the beginning, but without blocking the game from
  starting.

## [1.2.1] - 2023-02-01

### Changed

- Load levels when they are needed, not all at once at the beginning

## [1.2.0] - 2023-01-19

### Changed

- Count only the blocks you added, not the ones included in the level

## [1.1.2] - 2023-01-14

### Fixed

- Prevent buttons being squashed on iOS

## [1.1.1] - 2023-01-08

### Fixed

- Set the zoom level when you click Play in the level editor

## [1.1.0] - 2023-01-08

### Added

- "Focus" mode that removes almost all UI buttons
- Hover help on icon buttons

### Changed

- Remember the zoom position when editing levels

### Fixed

- Allow manually zooming out as far as some levels are set up for
- Improve auto-zoom of solution views

## [1.0.3] - 2023-01-05

### Fixed

- Prevent crash when there are no shapes in a level
- Fix: Crazy Golf solution uses blocks you are not allowed!
- Fix: In Up the instructions say "car" when they should say "ball"

### Changed

- Remember previous camera zoom and location when you switch between modes

## [1.0.2] - 2022-12-22

### Fixed

- Fixed a bug meaning solutions just show "Loading..." forever

## [1.0.1] - 2022-12-21

### Added

- Added version number to front page
- Added link to forum on front page

### Changed

- Changed timings to be more consistent

### Fixed

- Fixed solution viewer to display the full solution

## [1.0.0] - 2022-12-19

### Added

- The Game
